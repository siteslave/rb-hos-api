import * as knex from 'knex';

export class LoginModel {

  doLogin(db: knex, username: any, password: any) {
    const sql = db('opdconfig')
      .select('hospitalcode')
      .limit(1)
      .as('hospcode');

    return db('opduser')
      .select('name', 'loginname', sql)
      .where('loginname', username)
      .where('passweb', password);
  }
}