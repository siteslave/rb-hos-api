import * as knex from 'knex';

export class SurveilModel {

  getVisit(db: knex, vstdate: any) {
    const sql = `
    SELECT s.hn, s.cid, s.vn,concat(p.pname,p.fname," ",p.lname) as pt_name,
    concat(p.addrpart," หมู่ ",p.moopart," ",t.full_name) as address,
    s.vstdate,s.begin_date,s.report_date,s.pdx,
    i.name as icd_name,n.name as name506, s.code506,
    if(s.vn = v.vn,o.symptom,ip.prediag) as cc,o.pe,s.sv_number
    from surveil_member s
    join patient p on p.hn = s.hn
    left join vn_stat v on v.vn = s.vn
    left join an_stat a on a.an = s.vn
    left outer join thaiaddress t on t.chwpart = p.chwpart and t.amppart = p.amppart and t.tmbpart = p.tmbpart
    left outer join name506 n on n.code = s.code506
    left outer join icd101 i on i.code = s.pdx
    left outer join opdscreen o on o.vn = s.vn
    left outer join ipt ip on ip.an = s.vn
    where s.vstdate=?
    and left(s.pdx, 3) in ('A90', 'A91', 'A97')
    `;

    return db.raw(sql, [vstdate])
  }

  getLabs(db: knex, hn: any, startDate: any, endDate: any) {
    const sql = `
      select h.hn,h.order_date, h.order_time, h.lab_order_number from lab_order r
      inner join lab_head h on h.lab_order_number=r.lab_order_number
      inner join lab_items l on l.lab_items_code=r.lab_items_code 
      and l.provis_labcode in (0621201, 0622201, 0622601, 0749201, 0749200, 0749202)
      where hn=? and h.order_date between ? and ?
      group by lab_order_number
      order by h.order_date desc
    `;

    return db.raw(sql, [hn, startDate, endDate])
  }

  getLabResults(db: knex, orderNumber: any) {
    const sql = `
      select h.vn,h.hn, o.hos_guid, h.lab_order_number,
      h.order_date, h.order_time, o.lab_items_name_ref,
      o.lab_order_result, l.lab_items_normal_value, l.lab_items_unit
      from lab_head h
      join lab_order o on o.lab_order_number = h.lab_order_number
      join lab_items l on l.lab_items_code = o.lab_items_code
      where o.lab_order_number = ?
      and l.provis_labcode in (0621201, 0622201, 0622601, 0749201, 0749200, 0749202)
      and o.lab_order_result !=""
    `;

    return db.raw(sql, [orderNumber])
  }

  getPatientInfo(db: knex, svNumber: any) {
    const sql = `
    SELECT s.sv_number, s.vstdate, s.hn, s.vn, p.cid,
        concat(p.pname,p.fname," ",p.lname) as pt_name,
        s.vstdate,s.begin_date,s.report_date,s.pdx,
        i.name as icd_name,n.name as name506, s.code506,
        if(s.vn = v.vn,o.symptom,ip.prediag) as cc,o.pe,
        p.sex ,concat(p.addrpart," หมู่  ",p.moopart," ",t.full_name) as address,m.image,p.addrpart,p.moopart,p.tmbpart,p.amppart,p.chwpart
      ,p.occupation, op.name as occupation_name,p.birthday,s.addr as ill_addr,s.moo as ill_moo,s.tmbpart as ill_tmbpart,s.amppart as ill_amppart,s.chwpart as ill_chwpart,concat(s.addr,"  หมู่ ",s.moo," ",t2.full_name) as ill_address
        from surveil_member s
        join patient p on p.hn = s.hn
        left join vn_stat v on v.vn = s.vn
        left join an_stat a on a.an = s.vn
        left outer join name506 n on n.code = s.code506
        left outer join icd101 i on i.code = s.pdx
        left outer join opdscreen o on o.vn = s.vn
        left outer join ipt ip on ip.an = s.vn
        left outer join thaiaddress t on t.chwpart = p.chwpart and t.amppart = p.amppart and t.tmbpart = p.tmbpart
        left outer join thaiaddress t2 on t2.chwpart = s.chwpart and t2.amppart = s.amppart and t2.tmbpart = s.tmbpart
        left outer join patient_image m on m.hn = p.hn
        left outer join occupation op on op.occupation = p.occupation
  
        where s.sv_number = ?
    `;

    return db.raw(sql, [svNumber])
  }

  getChangwat(db: knex) {
    return db('thaiaddress')
      .select('name', 'chwpart')
      .where('codetype', '1')
      .whereNot('chwpart', '00')
      .orderBy('name');
  }

  getAmpur(db: knex, chwpart: any) {
    return db('thaiaddress')
      .select('name', 'chwpart', 'amppart')
      .where('codetype', '2')
      .where('chwpart', chwpart)
      .orderBy('amppart');
  }

  getTambol(db: knex, chwpart: any, amppart: any) {
    return db('thaiaddress')
      .select('name', 'chwpart', 'amppart', 'tmbpart')
      .where('codetype', '3')
      .where('chwpart', chwpart)
      .where('amppart', amppart)
      .orderBy('tmbpart');
  }


}