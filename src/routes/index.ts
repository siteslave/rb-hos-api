/// <reference path="../../typings.d.ts" />

import * as express from 'express';
import * as crypto from 'crypto';
import * as moment from 'moment';

import { Router, Request, Response } from 'express';
import * as HttpStatus from 'http-status-codes';
// import model
// import { TestModel } from "../models/test";
import { JwtModel } from "../models/jwt";
import { LoginModel } from '../models/login';

// const testModel = new TestModel();
const jwtModel = new JwtModel();

const loginModel = new LoginModel();

const router: Router = Router();

router.get('/', async (req: Request, res: Response) => {
  var token = jwtModel.sign({ hello: 'xxx' });
  console.log(token);
  res.send({ ok: true, message: 'Welcome to RESTful api server!', code: HttpStatus.OK });
});

router.post('/login', async (req: Request, res: Response) => {

  const db = req.db;

  const username = req.body.username;
  const password = req.body.password;

  if (username && password) {
    const encPass = crypto.createHash('md5').update(password).digest('hex');
    try {
      const rs: any = await loginModel.doLogin(db, username, encPass);
      if (rs.length) {
        const info: any = rs[0];
        const user: any = {};
        user.loginname = info.loginname;

        const hospcode = info.hospcode;
        const token = jwtModel.sign(user);
        res.send({ ok: true, token: token, fullname: info.name, hospcode: hospcode });
      } else {
        res.send({ ok: false, error: 'ชื่อผู้ใช้งานหรือรหัสผ่าน ไม่ถูกต้อง' })
      }
    } catch (error) {
      console.log(error);
      res.send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  } else {
    res.send({ ok: false, error: 'ข้อมูลไม่ครบ' })
  }
});


export default router;
