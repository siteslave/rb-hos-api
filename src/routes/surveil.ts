/// <reference path="../../typings.d.ts" />

import * as express from 'express';
import { Router, Request, Response } from 'express';
import * as HttpStatus from 'http-status-codes';
// import model

import * as moment from 'moment';
import { SurveilModel } from '../models/surveil';

// const testModel = new TestModel();
const surveilModel = new SurveilModel();

const router: Router = Router();

router.get('/list', async (req: Request, res: Response) => {

  const db = req.db;

  const ovstdate = req.query.ovstdate || moment().format('YYYY-MM-DD');

  try {
    const rs: any = await surveilModel.getVisit(db, ovstdate);
    const items = rs[0].map((v: any) => {
      v.vstdate = moment(v.vstdate).isValid() ? moment(v.vstdate).format('YYYY-MM-DD') : '';
      v.begin_date = moment(v.begin_date).isValid() ? moment(v.begin_date).format('YYYY-MM-DD') : '';
      v.report_date = moment(v.report_date).isValid() ? moment(v.report_date).format('YYYY-MM-DD') : '';

      return v;
    });
    res.send({ ok: true, rows: items });
  } catch (error) {
    console.log(error);
    res.send({ ok: false, error: 'เกิดข้อผิดพลาด [500]' });
  }

});

router.get('/labs', async (req: Request, res: Response) => {

  const db = req.db;

  const endDate = req.query.ovstdate; // endDate
  const startDate = moment(endDate, 'YYYY-MM-DD').subtract(10, 'day').format('YYYY-MM-DD');

  const hn = req.query.hn;

  if (hn && endDate) {
    try {
      const rs: any = await surveilModel.getLabs(db, hn, startDate, endDate);

      const items = rs[0].map(v => {
        v.order_date = moment(v.order_date).isValid() ? moment(v.order_date).format('YYYY-MM-DD') : '';
        return v;
      });

      res.send({ ok: true, rows: items });
    } catch (error) {
      console.log(error);
      res.send({ ok: false, error: 'เกิดข้อผิดพลาด [500]' });
    }
  } else {
    res.send({ ok: false, error: 'ข้อมูลไม่ครบ' });
  }


});

router.get('/lab-items', async (req: Request, res: Response) => {

  const db = req.db;

  const orderNumber = req.query.orderNumber;

  if (orderNumber) {
    try {
      const rs: any = await surveilModel.getLabResults(db, orderNumber);
      const items: any = rs[0].map((v: any) => {
        v.order_date = moment(v.order_date).isValid() ? moment(v.order_date).format('YYYY-MM-DD') : '';
        return v;
      })
      res.send({ ok: true, rows: items });
    } catch (error) {
      console.log(error);
      res.send({ ok: false, error: 'เกิดข้อผิดพลาด [500]' });
    }
  } else {
    res.send({ ok: false, error: 'ข้อมูลไม่ครบ' });
  }

});

router.get('/patient-info/:svNumber', async (req: Request, res: Response) => {

  const db = req.db;

  const svNumber = req.params.svNumber;

  try {
    const rs: any = await surveilModel.getPatientInfo(db, svNumber);

    const items = rs[0].map(v => {
      if (v.image) {
        v.image = v.image.toString("base64");
      }
      v.report_date = moment(v.report_date).isValid() ? moment(v.report_date).format('YYYY-MM-DD') : '';
      v.birthday = moment(v.birthday).isValid() ? moment(v.birthday).format('YYYY-MM-DD') : '';
      v.vstdate = moment(v.vstdate).isValid() ? moment(v.vstdate).format('YYYY-MM-DD') : '';
      v.begin_date = moment(v.begin_date).isValid() ? moment(v.begin_date).format('YYYY-MM-DD') : '';
      return v;
    });

    res.send({ ok: true, rows: items });
  } catch (error) {
    console.log(error);
    res.send({ ok: false, error: 'เกิดข้อผิดพลาด [500]' });
  }

});

router.get('/get-chw', async (req: Request, res: Response) => {

  const db = req.db;

  try {
    const rs: any = await surveilModel.getChangwat(db);
    res.send({ ok: true, rows: rs });
  } catch (error) {
    console.log(error);
    res.send({ ok: false, error: 'เกิดข้อผิดพลาด [500]' });
  }

});

router.get('/get-amp', async (req: Request, res: Response) => {

  const db = req.db;
  const chwpart = req.query.chwpart;

  try {
    const rs: any = await surveilModel.getAmpur(db, chwpart);
    res.send({ ok: true, rows: rs });
  } catch (error) {
    console.log(error);
    res.send({ ok: false, error: 'เกิดข้อผิดพลาด [500]' });
  }

});

router.get('/get-tmb', async (req: Request, res: Response) => {

  const db = req.db;
  const chwpart = req.query.chwpart;
  const amppart = req.query.amppart;

  try {
    const rs: any = await surveilModel.getTambol(db, chwpart, amppart);
    res.send({ ok: true, rows: rs });
  } catch (error) {
    console.log(error);
    res.send({ ok: false, error: 'เกิดข้อผิดพลาด [500]' });
  }

});



export default router;
